#include "Tutorial06_SpringsJointsApp.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include <glm\ext.hpp>
#include <Gizmos.h>

Tutorial06_SpringsJointsApp::Tutorial06_SpringsJointsApp() {

}

Tutorial06_SpringsJointsApp::~Tutorial06_SpringsJointsApp() {

}

bool Tutorial06_SpringsJointsApp::startup() {
	
	// Increase the 2d line count to maximize the number of objects we can draw
	aie::Gizmos::create(255U, 255U, 65535U, 65535U);

	m_2dRenderer = new aie::Renderer2D();

	// TODO: remember to change this when redistributing a build!
	// the following path would be used instead: "./font/consolas.ttf"
	m_font = new aie::Font("../bin/font/consolas.ttf", 32);

	m_physicsScene = new PhysicsScene();
	m_physicsScene->setTimeStep(0.01f);
	//Set Gravity to 0
	m_physicsScene->setGravity(glm::vec2(0, -10));

	int startX = -50;
	Sphere* ball1;
	Sphere* ball2;
	Sphere* ball3;
	float ballRadius = 2;
	float ballMass = 1;

	ball1 = new Sphere(glm::vec2(startX, 40), ballRadius);
	ball1->setMass(ballMass);
	ball1->setColour(glm::vec4(1, 0, 0, 1));
	ball1->setElasticity(0.9f);
	ball1->setKinematic(true);
	m_physicsScene->addActor(ball1);

	int numberOfBalls = 10;
	for (int i = 1; i < numberOfBalls; i++)
	{
		ball2 = new Sphere(glm::vec2(startX + i * 6.0f, 40), ballRadius);
		ball2->setMass(ballMass);
		ball2->setColour(glm::vec4(1, 0, 0, 1));
		ball2->setElasticity(0.9f);
		m_physicsScene->addActor(ball2);
		m_physicsScene->addActor(
			new Spring(ball1, ball2, 10, 10, 1.f));
		ball1 = ball2;
	}

	ball3 = new Sphere(glm::vec2(24, 40), ballRadius);
	ball3->setMass(ballMass);
	ball3->setElasticity(0.9f);
	ball3->setKinematic(true);
	m_physicsScene->addActor(ball3);
	m_physicsScene->addActor(
		new Spring(ball1, ball3, 5, 10, .1f));

	return true;
}

void Tutorial06_SpringsJointsApp::shutdown() {

	delete m_font;
	delete m_2dRenderer;
	delete m_physicsScene;
}

void Tutorial06_SpringsJointsApp::update(float deltaTime) {

	// input example
	aie::Input* input = aie::Input::getInstance();

	aie::Gizmos::clear();

	m_physicsScene->update(deltaTime);
	m_physicsScene->updateGizmos();
	m_physicsScene->checkForCollision();

	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void Tutorial06_SpringsJointsApp::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();

	// draw your stuff here!
	static float aspectRatio = 16 / 9.f;
	aie::Gizmos::draw2D(glm::ortho<float>(-100, 100, -100 / aspectRatio, 100 / aspectRatio, -1.0f, 1.0f));
	
	// output some text, uses the last used colour
	m_2dRenderer->drawText(m_font, "Press ESC to quit", 0, 0);

	// done drawing sprites
	m_2dRenderer->end();
}