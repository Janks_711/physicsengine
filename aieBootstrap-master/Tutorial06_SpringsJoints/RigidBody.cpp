#include "RigidBody.h"

const float RigidBody::MIN_LINEAR_THRESHOLD = 0.1f;
const float RigidBody::MIN_ROTATIONAL_THRESHOLD = 0.01f;

RigidBody::RigidBody(ShapeType shapeID, glm::vec2 position)
	: PhysicsObject(shapeID), m_position(position), m_velocity(glm::vec2(0, 0)), m_rotation(0.0f), m_mass(1.0f),
	m_linearDrag(0.3f), m_angularDrag(1.f), m_elasticity(1.f), m_angularVelocity(0.0f)
{
	m_isKinematic = false;
}

RigidBody::~RigidBody()
{
}

void RigidBody::fixedUpdate(glm::vec2 gravity, float timeStep)
{
	if (m_isKinematic)
		return;

	m_velocity += gravity * timeStep;
	m_position += m_velocity * timeStep;

	// Apply Rotation to the Rigid Body
	m_velocity -= m_velocity * m_linearDrag * timeStep;
	m_rotation += m_angularVelocity * timeStep;
	m_angularVelocity -= m_angularVelocity * m_angularDrag * timeStep;

	if (glm::length(m_velocity) < MIN_LINEAR_THRESHOLD)
	{
		if (glm::length(m_velocity) < glm::length(gravity) * m_linearDrag * timeStep)
		{
			m_velocity = glm::vec2(0, 0);
		}
	}	

	if (glm::abs(m_angularVelocity) < MIN_ROTATIONAL_THRESHOLD)
	{
		m_angularVelocity = 0;
	}
}

void RigidBody::debug()
{
}

void RigidBody::applyForce(glm::vec2 force, glm::vec2 pos)
{
	m_velocity += force / m_mass;
	m_angularVelocity += (force.y * pos.x - force.x * pos.y) / (m_moment);
}


void RigidBody::resolveCollision(RigidBody * actor2, glm::vec2 contact, glm::vec2* collisionNormal)
{
	// Find the vectore between their centres, or sue the provided direction
	// of force, and make sure it's normalised
	glm::vec2 normal = glm::normalize(collisionNormal ? *collisionNormal :
		actor2->m_position - m_position);

	// Get the vector perpendicular to the collision normal
	glm::vec2 perp(normal.y, -normal.x);

	// Determine the total velocity of the conact points for the two objects,
	// for both linear and rotational

	// 'r' is the radius from axis to application of force
	float r1 = glm::dot(contact - m_position, -perp);
	float r2 = glm::dot(contact - actor2->m_position, perp);

	// Velocity of the contact point on this object
	float v1 = glm::dot(m_velocity, normal) - r1 * m_angularVelocity;

	// Velocity of contact point on actor2
	float v2 = glm::dot(actor2->m_velocity, normal) + r2 * actor2->m_angularVelocity;

	if (v1 > v2) // They're moving closer
	{
		// Calculate the effective mass at contact point for each object
		// ie how much the contact point will move due to the force applied
		float mass1 = 1.0f / (1.0f / m_mass + (r1*r1) / m_moment);
		float mass2 = 1.0f / (1.0f / actor2->m_mass + (r2*r2) / actor2->m_moment);

		float elasticity = (m_elasticity + actor2->getElasticity()) / 2.0f;

		glm::vec2 force = (1.0f + elasticity) * mass1 * mass2 / (mass1 + mass2) * (v1 - v2) * normal;

		// Apply equal and opposite forces
		applyForce(-force, contact - m_position);
		actor2->applyForce(force, contact - actor2->m_position);
	}
}

// Use before and after collisons and compare the differnce
// Elasticty should be set to one for proper use
float RigidBody::getKineticEnergy()
{
	return 0.5f * (m_mass * glm::dot(m_velocity, m_velocity) + 
		m_moment * m_angularVelocity * m_angularVelocity);
}
