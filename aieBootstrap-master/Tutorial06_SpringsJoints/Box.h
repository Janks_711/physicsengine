#pragma once
#include "RigidBody.h"
class Box :
	public RigidBody
{
public:
	Box(glm::vec2 position,
		float width,
		float height);
	~Box();

	void fixedUpdate(glm::vec2 gravity, float timeStep);
	void makeGizmo();
	bool checkCollision(PhysicsObject* pOther);
	bool checkBoxCorners(const Box& box, glm::vec2& contact, int& numContacts, float &pen,
		glm::vec2& edgeNormal, glm::vec2& contactForce);

	float getWidth() { return m_extents.x; }
	float getHeight() { return m_extents.y; }

	const glm::vec2 getExtents() const { return m_extents; }

	const glm::vec2 getCenter() const 
	{ 
		return m_position;
	}

	glm::vec2 getLocalX() { return m_localX; }
	glm::vec2 getLocalY() { return m_localY; }

	void debug();

	virtual glm::vec2 toWorld(glm::vec2 other);

protected:
	// the halfedge lengths
	glm::vec2 m_extents;
	glm::vec4 m_colour;

	// Store the local x,y axes of the box based on its angle of rotation
	glm::vec2 m_localX;
	glm::vec2 m_localY;

	virtual void setMoment() { m_moment = 1.0f / 12.0f * m_mass * getWidth(), getHeight(); }
};

