#include "Tutorial06_SpringsJointsApp.h"

int main() {
	
	// allocation
	auto app = new Tutorial06_SpringsJointsApp();

	// initialise and loop
	app->run("AIE", 1280, 720, false);

	// deallocation
	delete app;

	return 0;
}