#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "PhysicsScene.h"
#include "Sphere.h"
#include "Plane.h"
#include "Box.h"
#include "Spring.h"

#define M_PI 3.1415926535897932384626

class Tutorial06_SpringsJointsApp : public aie::Application {
public:

	Tutorial06_SpringsJointsApp();
	virtual ~Tutorial06_SpringsJointsApp();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

protected:

	aie::Renderer2D*	m_2dRenderer;
	aie::Font*			m_font;
	PhysicsScene*		m_physicsScene;
};