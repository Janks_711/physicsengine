#include "Tutorial05_RotationalForceApp.h"

int main() {
	
	// allocation
	auto app = new Tutorial05_RotationalForceApp();

	// initialise and loop
	app->run("AIE", 1280, 720, false);

	// deallocation
	delete app;

	return 0;
}