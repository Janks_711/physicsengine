#pragma once
#include "PhysicsObject.h"

class RigidBody : public PhysicsObject {
public:
	RigidBody(ShapeType shapeID, glm::vec2 position, glm::vec2 velocity,
		float rotation, float mass, float linearDrag, float angularDrag, 
		float angularVelocity, float m_moment);
	~RigidBody();

	virtual void fixedUpdate(glm::vec2 gravity, float timeStep);
	virtual void debug();
	void applyForce(glm::vec2 force, glm::vec2 pos);

	virtual bool checkCollision(PhysicsObject* pOther) = 0;
	void resolveCollision(RigidBody* actor2, glm::vec2 contact, glm::vec2* collisionNormal = nullptr);

	glm::vec2 getPosition() { return m_position; }
	void setPosition(glm::vec2 value) { m_position = value; }

	float getRotation() { return m_rotation; }

	glm::vec2 getVelocity() { return m_velocity; }
	void setVelocity(glm::vec2 value) { m_velocity = value; }

	float getMass() { return m_mass; }
	void setMass(float value) { m_mass = value; }	
	
	float getLinearDrag() { return m_linearDrag; }
	void setLinearDrag(float value) { m_linearDrag = value; }
	
	float getAngularDrag() { return m_angularDrag; }
	void setAngularDrag(float value) { m_angularDrag = value; }	
	
	float getAngularVelocity() { return m_angularVelocity; }
	void setAngularVelocity(float value) { m_angularVelocity = value; }

	float getElasticity() { return m_elasticity; }
	void setElasticity(float value) { m_elasticity = value; }

	float getMoment() { return m_moment; }
	void setMoment(float value) { m_moment = value; }

	void setKinematic(bool value) { m_isKinematic = value; }

	float getKineticEnergy();

protected:
	glm::vec2 m_position;
	glm::vec2 m_velocity;
	float m_mass;
	float m_rotation; // how much the object has rotated in Radians
	float m_linearDrag;
	float m_angularDrag;
	float m_elasticity;
	float m_angularVelocity;
	float m_moment; // moment of inertia
	bool m_isKinematic;

private:
	static const float MIN_LINEAR_THRESHOLD;
	static const float MIN_ROTATIONAL_THRESHOLD;
}; 