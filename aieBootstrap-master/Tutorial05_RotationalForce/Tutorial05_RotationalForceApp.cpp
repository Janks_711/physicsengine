#include "Tutorial05_RotationalForceApp.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include <glm\ext.hpp>
#include <Gizmos.h>

Tutorial05_RotationalForceApp::Tutorial05_RotationalForceApp() {

}

Tutorial05_RotationalForceApp::~Tutorial05_RotationalForceApp() {

}

bool Tutorial05_RotationalForceApp::startup() {
	// Increase the 2d line count to maximize the number of objects we can draw
	aie::Gizmos::create(255U, 255U, 65535U, 65535U);

	m_2dRenderer = new aie::Renderer2D();

	// TODO: remember to change this when redistributing a build!
	// the following path would be used instead: "./font/consolas.ttf"
	m_font = new aie::Font("../bin/font/consolas.ttf", 32);

	m_physicsScene = new PhysicsScene();
	m_physicsScene->setTimeStep(0.01f);

	//Set Gravity to 0
	m_physicsScene->setGravity(glm::vec2(0, 0));

	float width = 4;
	float height = 2;
	float mass = 30;
	float rotation = 0;
	float linearDrag = 0.1f;
	float angularDrag = 0.2f;
	float elasticity = .3f;
	float angularVelocity = 0.3f;
	float moment = 10.f;
	glm::vec4 colour = glm::vec4(1, 0, 0, 1);

	Box* box = new Box(glm::vec2(20, 0), glm::vec2(-10, 0), width, height, mass, rotation,
		linearDrag, angularDrag, elasticity, angularVelocity, moment, colour);
	Box* box1 = new Box(glm::vec2(-20, 1), glm::vec2(10, 0), width, height, mass, rotation,
		linearDrag, angularDrag, elasticity, angularVelocity, moment, glm::vec4(0, 1, 0, 1));

	Sphere* sphere = new Sphere(glm::vec2(10, 0), glm::vec2(-20, 0), 30, 5, 0, .3f, .3f, 1.f, 0, 1.f, glm::vec4(0, 1, 0, 1));
	Sphere* sphere1 = new Sphere(glm::vec2(9, 0), glm::vec2(20, 0), 30, 5, 0, .3f, .3f, 1.f, 0, 1.f, glm::vec4(0, 0, 1, 1));

	Plane* plane = new Plane(glm::vec2(0, 1.f), 0);

	m_physicsScene->addActor(box);
	m_physicsScene->addActor(box1);

	return true;
}

void Tutorial05_RotationalForceApp::shutdown() {

	delete m_font;
	delete m_2dRenderer;
	delete m_physicsScene;
}

void Tutorial05_RotationalForceApp::update(float deltaTime) {

	// input example
	aie::Input* input = aie::Input::getInstance();

	aie::Gizmos::clear();

	m_physicsScene->update(deltaTime);
	m_physicsScene->updateGizmos();
	m_physicsScene->checkForCollision();

	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void Tutorial05_RotationalForceApp::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();

	// draw your stuff here!
	static float aspectRatio = 16 / 9.f;
	aie::Gizmos::draw2D(glm::ortho<float>(-100, 100, -100 / aspectRatio, 100 / aspectRatio, -1.0f, 1.0f));

	// output some text, uses the last used colour
	m_2dRenderer->drawText(m_font, "Press ESC to quit", 0, 0);

	// done drawing sprites
	m_2dRenderer->end();
}