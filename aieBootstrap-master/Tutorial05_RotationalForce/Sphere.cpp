#include "Sphere.h"
#include <Gizmos.h>

Sphere::Sphere(glm::vec2 position,
	glm::vec2 velocity,
	float mass,
	float radius,
	float rotation,
	float linearDrag,
	float angularDrag,
	float elasticity,
	float angularVelocity,
	float moment,
	glm::vec4 colour) :
	RigidBody(SPHERE, position,	velocity, rotation,	mass, linearDrag, angularDrag,
		angularVelocity, moment), m_radius(radius), m_colour(colour)
{
}

Sphere::~Sphere()
{
}

void Sphere::makeGizmo()
{
	glm::vec2 end = glm::vec2(std::cos(m_rotation), std::sin(m_rotation)) * m_radius;

	aie::Gizmos::add2DCircle(m_position, m_radius, 100, m_colour);
	aie::Gizmos::add2DLine(m_position, m_position + end, glm::vec4(1, 1, 1, 1));
}

bool Sphere::checkCollision(PhysicsObject * pOther)
{
	Sphere* oSphere = dynamic_cast<Sphere*>(pOther);

	if (oSphere) {
		float distance = glm::distance(this->m_position, oSphere->getPosition());
		float combineRad = this->m_radius + oSphere->getRadius();

		if (distance < combineRad)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	return false;
}
