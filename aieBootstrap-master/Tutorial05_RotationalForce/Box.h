#pragma once
#include "RigidBody.h"
class Box :
	public RigidBody
{
public:
	Box(glm::vec2 position,
		glm::vec2 velocity,
		float width,
		float height,
		float mass,
		float rotation,
		float linearDrag,
		float angularDrag,
		float elasticity,
		float angularVelocity,
		float moment,
		glm::vec4 colour);
	~Box();

	void fixedUpdate(glm::vec2 gravity, float timeStep);
	void makeGizmo();
	bool checkCollision(PhysicsObject* pOther);
	bool checkBoxCorners(const Box& box, glm::vec2& contact, int& numContacts, float &pen,
		glm::vec2& edgeNormal, glm::vec2& contactForce);

	float getWidth() { return m_extents.x * 2; }
	float getHeight() { return m_extents.y * 2; }

	const glm::vec2 getExtents() const { return m_extents; }

	const glm::vec2 getCenter() const 
	{ 
		return m_position;
	}

	glm::vec2 getLocalX() { return m_localX; }
	glm::vec2 getLocalY() { return m_localY; }

	void debug();

protected:
	// the halfedge lengths
	glm::vec2 m_extents;
	glm::vec4 m_colour;

	// Store the local x,y axes of the box based on its angle of rotation
	glm::vec2 m_localX;
	glm::vec2 m_localY;
};

