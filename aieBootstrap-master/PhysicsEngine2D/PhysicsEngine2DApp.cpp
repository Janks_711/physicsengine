#include "PhysicsEngine2DApp.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include <glm\ext.hpp>
#include <Gizmos.h>

PhysicsEngine2DApp::PhysicsEngine2DApp() {

}

PhysicsEngine2DApp::~PhysicsEngine2DApp() {

}

bool PhysicsEngine2DApp::startup() {

	// Increase the 2d line count to maximize the number of objects we can draw
	aie::Gizmos::create(255U, 255U, 65535U, 65535U);

	m_2dRenderer = new aie::Renderer2D();

	// TODO: remember to change this when redistributing a build!
	// the following path would be used instead: "./font/consolas.ttf"
	m_font = new aie::Font("../bin/font/consolas.ttf", 32);

	m_physicsScene = new PhysicsScene();	
	m_physicsScene->setTimeStep(0.01f);
	//Set Gravity to 0
	//m_physicsScene->setGravity(glm::vec2(0, 0.0f));
	m_physicsScene->setGravity(glm::vec2(0, -2.81f));

	Sphere* ball1;
	Sphere* ball2;
	Box* box1;
	Box* box2;

	ball1 = new Sphere(glm::vec2(0, 50.0f), 5);
	ball1->setMass(1);
	ball1->setElasticity(.8f);
	//ball1->setVelocity(glm::vec2(80, 0));
	ball1->applyForce(glm::vec2(-20.0f, -30.0f), ball1->getPosition());
	m_physicsScene->addActor(ball1);

	ball2 = new Sphere(glm::vec2(-0.5f, 0), 5);
	ball2->setMass(1);
	ball2->setElasticity(.8f);
	ball2->setColour(glm::vec4(0, 1, 0, 1));
	ball2->applyForce(glm::vec2(20.f, 15.f), ball2->getPosition());
	//ball2->setKinematic(true);
	m_physicsScene->addActor(ball2);

	box1 = new Box(glm::vec2(0, -30.0f), 5, 2);
	box1->setMass(1);
	box1->setElasticity(.6f);
	box1->applyForce(glm::vec2(-20, 5), box1->getPosition());
	box1->setColour(glm::vec4(0, 0, 1, 1));
	//box1->setVelocity(glm::vec2(-20, 0));
	//box1->setKinematic(true);
	m_physicsScene->addActor(box1);	
	
	box2 = new Box(glm::vec2(20, 10), 5, 2);
	box2->setMass(1);
	box2->setElasticity(.6f);
	box2->applyForce(glm::vec2(20, 25), box2->getPosition());
	box2->setColour(glm::vec4(0, 1, 0, 1));
	//box2->setKinematic(true);
	m_physicsScene->addActor(box2);

	Plane* planeTop;
	Plane* planeRight;
	Plane* planeLeft;
	Plane* planeBottom;

	planeTop = new Plane(glm::vec2(0, -1.f), -50);
	m_physicsScene->addActor(planeTop);
	
	planeRight = new Plane(glm::vec2(-1.0f, 0), -90);
	m_physicsScene->addActor(planeRight);	
	
	planeLeft = new Plane(glm::vec2(1.0f, 0), -90);
	m_physicsScene->addActor(planeLeft);	
	
	planeBottom = new Plane(glm::vec2(0.0f, 1.f), -50);
	m_physicsScene->addActor(planeBottom);


	return true;
}

void PhysicsEngine2DApp::shutdown() {

	delete m_font;
	delete m_2dRenderer;
	delete m_physicsScene;
}

void PhysicsEngine2DApp::update(float deltaTime) {

	// input example
	aie::Input* input = aie::Input::getInstance();

	aie::Gizmos::clear();

	m_physicsScene->update(deltaTime);
	m_physicsScene->updateGizmos();
	m_physicsScene->checkForCollision();

	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void PhysicsEngine2DApp::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();

	// draw your stuff here!
	static float aspectRatio = 16 / 9.f;
	aie::Gizmos::draw2D(glm::ortho<float>(-100, 100, -100 / aspectRatio, 100 / aspectRatio, -1.0f, 1.0f));

	// output some text, uses the last used colour
	m_2dRenderer->drawText(m_font, "Press ESC to quit", 0, 0);

	// done drawing sprites
	m_2dRenderer->end();
}