#include "Box.h"
#include <Gizmos.h>
#include <iostream>

Box::Box(glm::vec2 position, float width, float height)
	: RigidBody(ShapeType::BOX, position), 
	m_extents(glm::vec2(width, height)), m_colour(glm::vec4(1, 0, 0, 1))
{
	float cs = cosf(m_rotation);
	float sn = sinf(m_rotation);
	m_localX = glm::normalize(glm::vec2(cs, sn));
	m_localY = glm::normalize(glm::vec2(-sn, cs));

	setMoment();
}

Box::~Box()
{
}

void Box::fixedUpdate(glm::vec2 gravity, float timeStep)
{
	RigidBody::fixedUpdate(gravity, timeStep);

	//store the local axes
	float cs = cosf(m_rotation);
	float sn = sinf(m_rotation);
	m_localX = glm::normalize(glm::vec2(cs, sn));
	m_localY = glm::normalize(glm::vec2(-sn, cs));
}

void Box::makeGizmo() 
{
	// if only using rotation
	//glm::mat4 tranform = glm::rotate(m_rotation, glm::vec3(0, 0, 1));
	//aie::Gizmos::add2DAABBFilled(getCenter(), m_extents, m_colour, &tranform);

	//Draw using local axes
	glm::vec2 p1 = m_position - m_localX * m_extents.x - m_localY * m_extents.y;
	glm::vec2 p2 = m_position + m_localX * m_extents.x - m_localY * m_extents.y;
	glm::vec2 p3 = m_position - m_localX * m_extents.x + m_localY * m_extents.y;
	glm::vec2 p4 = m_position + m_localX * m_extents.x + m_localY * m_extents.y;
	aie::Gizmos::add2DTri(p1, p2, p4, m_colour);
	aie::Gizmos::add2DTri(p1, p4, p3, m_colour);
}

bool Box::checkCollision(PhysicsObject * pOther)
{
	return false;
}

bool Box::checkBoxCorners(const Box & box, glm::vec2 & contact, int & numContacts, float& pen,
	glm::vec2 & edgeNormal)
{
	float minX, maxX, minY, maxY;
	float boxW = box.getExtents().x * 2;
	float boxH = box.getExtents().y * 2;
	float numLocalContacts = 0;
	glm::vec2 localContact(0, 0);

	bool first = true;
	for (float x = -box.getExtents().x; x < boxW; x += boxW)
	{
		for (float y = -box.getExtents().y; y < boxH; y += boxH)
		{
			// Position in world Space
			glm::vec2 p = box.getCenter() + x * box.m_localX + y * box.m_localY;

			// Position in our box's space
			glm::vec2 p0(glm::dot(p - m_position, m_localX),
				glm::dot(p - m_position, m_localY));

			if (first || p0.x < minX) minX = p0.x;
			if (first || p0.x > maxX) maxX = p0.x;
			if (first || p0.y < minY) minY = p0.y;
			if (first || p0.y > maxY) maxY = p0.y;

			if (p0.x >= -m_extents.x && p0.x <= m_extents.x && 
				p0.y >= -m_extents.y && p0.y <= m_extents.y)
			{
				numLocalContacts++;
				localContact += p0;
			}
			first = false;
		}
	}

	if (maxX <= -m_extents.x || minX >= m_extents.x || 
		maxY <= -m_extents.y || minY >= m_extents.y)
	{
		return false;
	}
	
	if (numLocalContacts == 0)
	{
		return false;
	}
	
	bool res = false;
	
	contact += m_position + (localContact.x * m_localX + localContact.y * m_localY) / 
		(float)numLocalContacts;
	numContacts++;
	
	float pen0 = m_extents.x - minX;
	if (pen0 > 0 && (pen0 < pen || pen == 0))
	{
		edgeNormal = m_localX;
		pen = pen0;
		res = true;
	}	
	
	pen0 = maxX + m_extents.x;
	if (pen0 > 0 && (pen0 < pen || pen == 0))
	{
		edgeNormal = -m_localX;
		pen = pen0;
		res = true;
	}	
	
	pen0 = m_extents.y - minY;
	if (pen0 > 0 && (pen0 < pen || pen == 0))
	{
		edgeNormal = m_localY;
		pen = pen0;
		res = true;
	}
	
	pen0 = maxY + m_extents.y;
	if (pen0 > 0 && (pen0 < pen || pen == 0))
	{
		edgeNormal = -m_localY;
		pen = pen0;
		res = true;
	}
	
	return res;

	//for (float x = -box.getExtents().x; x < boxW; x += boxW)
	//{
	//	for (float y = -box.getExtents().y; y < boxH; y += boxH)
	//	{
	//		// Position in World Space
	//		glm::vec2 boxInWorldSpace = box.m_position + x * box.m_localX + y * box.m_localY;

	//		// Position in our box's space
	//		glm::vec2 ourBoxInWorldSpace(glm::dot(boxInWorldSpace - m_position, m_localX),
	//			glm::dot(boxInWorldSpace - m_position, m_localY));

	//		float w2 = m_extents.x, h2 = m_extents.y;

	//		if (ourBoxInWorldSpace.y < h2 && ourBoxInWorldSpace.y > -h2)
	//		{
	//			if (ourBoxInWorldSpace.x > 0 && ourBoxInWorldSpace.x < w2)
	//			{
	//				numContacts++;
	//				contact += m_position + w2 * m_localX + ourBoxInWorldSpace.y * m_localY;
	//				edgeNormal = m_localX;
	//				penetration = w2 - ourBoxInWorldSpace.x;
	//			}
	//			if (ourBoxInWorldSpace.x < 0 && ourBoxInWorldSpace.x > -w2)
	//			{
	//				numContacts++;
	//				contact += m_position - w2 * m_localX + ourBoxInWorldSpace.y * m_localY;
	//				edgeNormal = -m_localX;
	//				penetration = w2 + ourBoxInWorldSpace.x;
	//			}
	//		}

	//		if (ourBoxInWorldSpace.x < w2 && ourBoxInWorldSpace.x > -w2)
	//		{
	//			if (ourBoxInWorldSpace.y > 0 && ourBoxInWorldSpace.y < h2)
	//			{
	//				numContacts++;
	//				contact += m_position + ourBoxInWorldSpace.x * m_localX + h2 * m_localY;
	//				float pen0 = h2 - ourBoxInWorldSpace.y;
	//				if (pen0 < penetration || penetration == 0)
	//				{
	//					penetration = pen0;
	//					edgeNormal = m_localY;
	//				}
	//			}
	//			if (ourBoxInWorldSpace.y < 0 && ourBoxInWorldSpace.y > -h2)
	//			{
	//				numContacts++;
	//				contact += m_position + ourBoxInWorldSpace.x * m_localX - h2 * m_localY;
	//				float pen0 = h2 + ourBoxInWorldSpace.y;
	//				if (pen0 < penetration || penetration == 0)
	//				{
	//					penetration = pen0;
	//					edgeNormal = -m_localY;
	//				}
	//			}
	//		}
	//	}
	//}
	//contactForce = penetration * edgeNormal;
	//return (penetration != 0);


}

void Box::debug()
{
	std::cout << getCenter().x << ", " << getCenter().y << std::endl;
}

glm::vec2 Box::toWorld(glm::vec2 other)
{
	return m_position + getLocalX() * other.x + getLocalY() * other.y;
}
