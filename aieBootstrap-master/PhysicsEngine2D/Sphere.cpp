#include "Sphere.h"
#include <Gizmos.h>

Sphere::Sphere(glm::vec2 position, float radius)
	: RigidBody(ShapeType::SPHERE, position), m_radius(radius), m_colour(glm::vec4(1, 0, 0, 1))
{
	setMoment();
}

Sphere::~Sphere()
{
}

void Sphere::makeGizmo()
{
	glm::vec2 end = glm::vec2(std::cos(m_rotation), std::sin(m_rotation)) * m_radius;

	aie::Gizmos::add2DCircle(m_position, m_radius, 100, m_colour);
	aie::Gizmos::add2DLine(m_position, m_position + end, glm::vec4(1, 1, 1, 1));
}

bool Sphere::checkCollision(PhysicsObject * pOther)
{
	Sphere* oSphere = dynamic_cast<Sphere*>(pOther);

	if (oSphere) {
		float distance = glm::distance(this->m_position, oSphere->getPosition());
		float combineRad = this->m_radius + oSphere->getRadius();

		if (distance < combineRad)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	return false;
}

glm::vec2 Sphere::toWorld(glm::vec2 other)
{
	return m_position;
}
