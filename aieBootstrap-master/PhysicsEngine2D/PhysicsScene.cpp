#include "PhysicsScene.h"
#include <algorithm>
#include <list>
#include "Sphere.h"
#include "Plane.h"
#include "Box.h"
#include <iostream>


PhysicsScene::PhysicsScene() : m_timeStep(0.01f), m_gravity(0)
{
}


PhysicsScene::~PhysicsScene()
{
	for (auto pActor : m_actors)
	{
		delete pActor;
	}
}

void PhysicsScene::addActor(PhysicsObject * actor)
{
	m_actors.push_back(actor);
}

void PhysicsScene::removeActor(PhysicsObject * actor)
{
	m_actors.erase(std::remove(m_actors.begin(), m_actors.end(), actor), m_actors.end());
}

void PhysicsScene::update(float dt)
{
	// Update Physics at a fixed TimeStep
	static float accumulatedTime = 0.0f;
	accumulatedTime += dt;

	while (accumulatedTime >= m_timeStep)
	{
		for (auto pActor : m_actors)
		{
			pActor->fixedUpdate(m_gravity, m_timeStep);
		}

		accumulatedTime -= m_timeStep;	
	}
}

void PhysicsScene::updateGizmos()
{
	for (auto pActor : m_actors) {
		pActor->makeGizmo();
	}
}

void PhysicsScene::debugScene()
{
	int count = 0;
	for (auto pActor : m_actors)
	{
		std::cout << count << " : ";
		pActor->debug();
		count++;
	}
}

typedef bool(*fn)(PhysicsObject*, PhysicsObject*);

static fn collisionFunctionArray[] = 
{
	PhysicsScene::plane2Plane, PhysicsScene::plane2Sphere, PhysicsScene::plane2Box,
	PhysicsScene::sphere2Plane, PhysicsScene::sphere2Sphere, PhysicsScene::sphere2Box, 
	PhysicsScene::box2Plane, PhysicsScene::box2Sphere, PhysicsScene::box2Box,  
};

void PhysicsScene::checkForCollision()
{
	int actorCount = m_actors.size();

	// Need to check for collisions against all objects except this one
	for (int outer = 0; outer < actorCount - 1; outer++)
	{
		for (int inner = outer + 1; inner < actorCount; inner++)
		{
			PhysicsObject* object1 = m_actors[outer];
			PhysicsObject* object2 = m_actors[inner];
			int shapeId1 = object1->getShapeID();
			int shapeId2 = object2->getShapeID();

			// This check will ensure we don't include any joints in the collision checks
			if (shapeId1 < 0 || shapeId2 < 0)
			{
				continue;
			}

			// Using function pointers
			int functionIdx = (shapeId1 * SHAPE_COUNT) + shapeId2;
			fn collisionFunctionPtr = collisionFunctionArray[functionIdx];
			if (collisionFunctionPtr != nullptr)
			{
				RigidBody* obj1 = dynamic_cast<RigidBody*>(object1);
				RigidBody* obj2 = dynamic_cast<RigidBody*>(object2);

				float kePre = 0.0f;
				float kePost = 0.0f;
				float deltaKe = 0.0f;

				if (obj1 != nullptr && obj2 != nullptr)
				{
					kePre = obj1->getKineticEnergy() + obj2->getKineticEnergy();
				}

				// did a collision occur?
				collisionFunctionPtr(object1, object2);

				if (obj1 != nullptr && obj2 != nullptr)
				{
					kePost = obj1->getKineticEnergy() + obj2->getKineticEnergy();

					deltaKe = kePost - kePre;
					if (deltaKe < -0.01f || deltaKe > 0.01f)
					{
						std::cout << "Energy Change detected!" << std::endl;
					}
				}
			}
		}
	}
}

void PhysicsScene::ApplyContactForces(RigidBody* body1, RigidBody* body2, glm::vec2 normal, float pen)
{
	float body1Factor = body1->isKinematic() ? 0 : (body2->isKinematic() ? 1.0f : 0.5f);

	body1->setPosition(body1->getPosition() - body1Factor * normal * pen);
	body2->setPosition(body2->getPosition() + (1 - body1Factor) * normal * pen);
}


#pragma region PlanetoObjectCollisions
bool PhysicsScene::plane2Plane(PhysicsObject *, PhysicsObject *)
{
	return false;
}

bool PhysicsScene::plane2Sphere(PhysicsObject * obj1, PhysicsObject * obj2)
{
	Plane* plane = dynamic_cast<Plane*>(obj1);
	Sphere* sphere = dynamic_cast<Sphere*>(obj2);

	// if we are successful then test for collision
	if (sphere != nullptr && plane != nullptr)
	{
		glm::vec2 collisionNormal = plane->getNormal();
		float sphereToPlane = glm::dot(
			sphere->getPosition(),
			plane->getNormal()) - plane->getDistance();

		// if we are behind the plane then we flip the normal
		if (sphereToPlane < 0) {
			collisionNormal *= -1;
			sphereToPlane *= -1;
		}

		float intersection = sphere->getRadius() - sphereToPlane;
		if (intersection > 0) {

			// Contact Forces
			sphere->setPosition(sphere->getPosition() + plane->getNormal() *
				(sphere->getRadius() - sphereToPlane));
			
			glm::vec2 contact = sphere->getPosition() + (collisionNormal * -sphere->getRadius());

			plane->resolveCollision(sphere, contact);

			return true;
		}
	}

	return false;
}

bool PhysicsScene::plane2Box(PhysicsObject * obj1, PhysicsObject * obj2)
{
	Plane *plane = dynamic_cast<Plane*>(obj1);
	Box *box = dynamic_cast<Box*>(obj2);

	// If we are successful then test for collision
	if (box != nullptr && plane != nullptr)
	{
		int numContacts = 0;
		glm::vec2 contact(0, 0);
		float contactV = 0;
		float radius = 0.5f * std::fminf(box->getWidth(), box->getHeight());
		float penetration = 0;

		//Which side is the Center of mass on?
		glm::vec2 planeOrigin = plane->getNormal() * plane->getDistance();
		float comFromPLane = glm::dot(box->getPosition() - planeOrigin, plane->getNormal());

		// Check all four corners to see if we've hit the plane
		for (float x = -box->getExtents().x; x < box->getWidth(); x += box->getWidth())
		{
			for (float y = -box->getExtents().y; y < box->getHeight(); y += box->getHeight())
			{
				glm::vec2 p = box->getPosition() + x * box->getLocalX() + y * box->getLocalY();
				float distFromPlane = glm::dot(p - planeOrigin, plane->getNormal());

				// This is the total velocity of the point
				float velocityIntoPlane = glm::dot(box->getVelocity() + box->getAngularVelocity() *
					(-y * box->getLocalX() + x * box->getLocalY()), plane->getNormal());

				// If this corner is on the opposite side from the COM,
				// and moving further in, we need to resolve the collision
				if ((distFromPlane > 0 && comFromPLane < 0 && velocityIntoPlane >= 0) ||
					(distFromPlane < 0 && comFromPLane > 0 && velocityIntoPlane <= 0))
				{
					numContacts++;
					contact += p;
					contactV += velocityIntoPlane;

					if (comFromPLane >= 0)
					{
						if (penetration > distFromPlane)
						{
							penetration = distFromPlane;
						}
					}
					else
					{
						if (penetration < distFromPlane)
						{
							penetration = distFromPlane;
						}
					}
				}
			}
		}

		// If we've had a hit - typically only two corners can contact
		if (numContacts > 0)
		{
			// Get the average collision velocity into the plane (covers linear and rotationa;
			// velocity of all corners involved)
			float collisionV = contactV / (float)numContacts;

			// Get the acceleration required to stop (restitution = 0) or reverse (restitution= 1)
			// the average velocity into the plane
			glm::vec2 acceleration = -plane->getNormal() * ((1.0f + box->getElasticity())* collisionV);

			// And the average position at which we'll apply the force (corner or edge centre)
			glm::vec2 localContact = (contact / (float)numContacts) - box->getPosition();

			// This is the perpendicular distance we apply the force at relative to the COM, so Torque = F*r
			float r = glm::dot(localContact, glm::vec2(plane->getNormal().y, -plane->getNormal().x));

			// Work out the "effective mass" - this is a combination of moment of inertia and mass,
			// and tells us how much the contact point velocity will change with the force we are applying
			float mass0 = 1.0f / (1.0f / box->getMass() + (r*r) / box->getMoment());

			// Apply Force
			box->applyForce(acceleration * mass0, localContact);
			box->setPosition(box->getPosition() - plane->getNormal() * penetration);
		}
	}
	return false;
}
#pragma endregion

#pragma region SphereToObjectCollisions
bool PhysicsScene::sphere2Plane(PhysicsObject * obj1, PhysicsObject * obj2)
{
	Sphere* sphere = dynamic_cast<Sphere*>(obj1);
	Plane* plane = dynamic_cast<Plane*>(obj2);

	// if we are successful then test for collision
	if (sphere != nullptr && plane != nullptr)
	{
		glm::vec2 collisionNormal = plane->getNormal();
		float sphereToPlane = glm::dot(
			sphere->getPosition(),
			plane->getNormal()) - plane->getDistance();

		// if we are behind the plane then we flip the normal
		if (sphereToPlane < 0) {
			collisionNormal *= -1;
			sphereToPlane *= -1;
		}

		float intersection = sphere->getRadius() - sphereToPlane;
		if (intersection > 0) {

			// Contact Force
			sphere->setPosition(sphere->getPosition() + plane->getNormal() *
				(sphere->getRadius() - sphereToPlane));

			glm::vec2 contact = sphere->getPosition() + (collisionNormal * -sphere->getRadius());
			
			plane->resolveCollision(sphere, contact);

			return true;
		}
	}

	return false;
}

bool PhysicsScene::sphere2Sphere(PhysicsObject * obj1, PhysicsObject * obj2)
{
	//try to cast objects to sphere and sphere
	Sphere *sphere1 = dynamic_cast<Sphere*>(obj1);
	Sphere *sphere2 = dynamic_cast<Sphere*>(obj2);
	//if we are successful then test for collision
	if (sphere1 != nullptr && sphere2 != nullptr)
	{
		glm::vec2 delta = sphere2->getPosition() - sphere1->getPosition();
		float distance = glm::length(delta);
		float intersection = sphere1->getRadius() + sphere2->getRadius() - distance;

		// If intersection is 0 There is a collision
		if (intersection > 0)
		{
			// Calculate the contact force of each sphere
			glm::vec2 contactForce = 0.5f * (distance - (sphere1->getRadius() +
				sphere2->getRadius())) * delta / distance;

			sphere1->setPosition(sphere1->getPosition() + contactForce);
			sphere2->setPosition(sphere2->getPosition() - contactForce);

			glm::vec2 contact = 0.5f * (sphere1->getPosition() + sphere2->getPosition());
			
			float pen = sphere2->getRadius() - glm::length(contact - sphere2->getPosition());
			glm::vec2 penVec = glm::normalize(contact - sphere2->getPosition()) * pen;

			if (!sphere1->isKinematic() && !sphere2->isKinematic())
			{
				sphere1->setPosition(sphere1->getPosition() + penVec * 0.5f);
				sphere2->setPosition(sphere2->getPosition() - penVec * 0.5f);
			}
			else if (!sphere1->isKinematic())
			{
				sphere1->setPosition(sphere1->getPosition() + penVec);
			}
			else
			{
				sphere2->setPosition(sphere2->getPosition() - penVec);
			}
			
			sphere1->resolveCollision(sphere2, contact);
			return true;
		}
	}
	return false;
}

bool PhysicsScene::sphere2Box(PhysicsObject *obj1, PhysicsObject *obj2)
{
	// We have a collision when:
// 1. A Corner of the box is inside the circle
// 2. The circle has touched one of the edges of the box

	Sphere* sphere = dynamic_cast<Sphere*>(obj1);
	Box* box = dynamic_cast<Box*>(obj2);

	if (box != nullptr && sphere != nullptr)
	{
		// Checking first condition
		glm::vec2 circlePos = sphere->getPosition() - box->getPosition();
		float w2 = box->getWidth() / 2, h2 = box->getHeight() / 2;

		int numContacts = 0;
		glm::vec2 contact(0, 0); // contact is in our box coordintates

		//check the four corners to see if any of them are inside the circle
		for (float x = -w2; x <= w2; x += box->getWidth())
		{
			for (float y = -h2; y <= h2; y += box->getHeight())
			{
				glm::vec2 p = x * box->getLocalX() + y * box->getLocalY();
				glm::vec2 dp = p - circlePos;

				if (dp.x * dp.x + dp.y * dp.y < sphere->getRadius() * sphere->getRadius())
				{
					numContacts++;
					contact += glm::vec2(x, y);
				}
			}
		}

		// Checkeing second condition
		glm::vec2* direction = nullptr;

		// Get the local positon of the circle centre
		glm::vec2 localPos(glm::dot(box->getLocalX(), circlePos),
			glm::dot(box->getLocalY(), circlePos));

		if (localPos.y < h2 && localPos.y > -h2)
		{
			if (localPos.x > 0 && localPos.x < w2 + sphere->getRadius())
			{
				numContacts++;
				contact += glm::vec2(w2, localPos.y);
				direction = new glm::vec2(box->getLocalX());
			}
			if (localPos.x < 0 && localPos.x > -(w2 + sphere->getRadius()))
			{
				numContacts++;
				contact += glm::vec2(-w2, localPos.y);
				direction = new glm::vec2(-box->getLocalX());
			}
		}

		if (localPos.x < w2 && localPos.x > -w2)
		{
			if (localPos.y > 0 && localPos.y < h2 + sphere->getRadius())
			{
				numContacts++;
				contact += glm::vec2(localPos.x, h2);
				direction = new glm::vec2(box->getLocalY());
			}
			if (localPos.y < 0 && localPos.y > -(h2 + sphere->getRadius()))
			{
				numContacts++;
				contact += glm::vec2(localPos.x, -h2);
				direction = new glm::vec2(-box->getLocalY());
			}
		}

		if (numContacts > 0)
		{
			contact = box->getPosition() + (1.0f / numContacts) * (box->getLocalX()*contact.x + box->getLocalY()*contact.y);

			// With the contact point we can find a penetration vector
			float pen = sphere->getRadius() - glm::length(contact - sphere->getPosition());
			glm::vec2 norm = glm::normalize(sphere->getPosition() - contact);
			glm::vec2 penVec = glm::normalize(contact - sphere->getPosition()) * pen;

			box->resolveCollision(sphere, contact, direction);
			ApplyContactForces(box, sphere, norm, pen);
		}
		delete direction;
	}
	return false;
}
#pragma endregion

#pragma region BoxToObjectCollisions
bool PhysicsScene::box2Plane(PhysicsObject * obj1, PhysicsObject * obj2)
{
	Box *box = dynamic_cast<Box*>(obj1);
	Plane *plane = dynamic_cast<Plane*>(obj2);

	// If we are successful then test for collision
	if (box != nullptr && plane != nullptr)
	{
		int numContacts = 0;
		glm::vec2 contact(0, 0);
		float contactV = 0;
		float radius = 0.5f * std::fminf(box->getWidth(), box->getHeight());
		float penetration = 0;

		//Which side is the Center of mass on?
		glm::vec2 planeOrigin = plane->getNormal() * plane->getDistance();
		float comFromPlane = glm::dot(box->getPosition() - planeOrigin, plane->getNormal());

		// Check all four corners to see if we've hit the plane
		for (float x = -box->getExtents().x; x < box->getWidth(); x += box->getWidth())
		{
			for (float y = -box->getExtents().y; y < box->getHeight(); y += box->getHeight())
			{
				glm::vec2 p = box->getPosition() + x * box->getLocalX() + y * box->getLocalY();
				float distFromPlane = glm::dot(p - planeOrigin, plane->getNormal());

				// This is the total velocity of the point
				float velocityIntoPlane = glm::dot(box->getVelocity() + box->getAngularVelocity() *
					(-y * box->getLocalX() + x * box->getLocalY()), plane->getNormal());

				// If this corner is on the opposite side from the COM,
				// and moving further in, we need to resolve the collision
				if ((distFromPlane > 0 && comFromPlane < 0 && velocityIntoPlane > 0) ||
					(distFromPlane < 0 && comFromPlane > 0 && velocityIntoPlane < 0))
				{
					numContacts++;
					contact += p;
					contactV += velocityIntoPlane;

					if (comFromPlane >= 0)
					{
						if (penetration > distFromPlane)
						{
							penetration = distFromPlane;
						}
					}
					else
					{
						if (penetration < distFromPlane)
						{
							penetration = distFromPlane;
						}
					}
				}
			}
		}

		// If we've had a hit - typically only two corners can contact
		if (numContacts > 0)
		{
			// Get the average collision velocity into the plane (covers linear and rotationa;
			// velocity of all corners involved)
			float collisionV = contactV / (float)numContacts;

			// Get the acceleration required to stop (restitution = 0) or reverse (restitution= 1)
			// the average velocity into the plane
			glm::vec2 acceleration = -plane->getNormal() * ((1.0f + box->getElasticity())* collisionV);

			// And the average position at which we'll apply the force (corner or edge centre)
			glm::vec2 localContact = (contact / (float)numContacts) - box->getPosition();

			// This is the perpendicular distance we apply the force at relative to the COM, so Torque = F*r
			float r = glm::dot(localContact, glm::vec2(plane->getNormal().y, -plane->getNormal().x));

			// Work out the "effective mass" - this is a combination of moment of inertia and mass,
			// and tells us how much the contact point velocity will change with the force we are applying
			float mass0 = 1.0f / (1.0f / box->getMass() + (r*r) / box->getMoment());

			// Apply Force
			box->applyForce(acceleration * mass0, localContact);
			box->setPosition(box->getPosition() - plane->getNormal() * penetration);
		}
	}

	return false;
}

bool PhysicsScene::box2Sphere(PhysicsObject * obj1, PhysicsObject * obj2)
{
	// We have a collision when:
	// 1. A Corner of the box is inside the circle
	// 2. The circle has touched one of the edges of the box

	Box* box = dynamic_cast<Box*>(obj1);
	Sphere* sphere = dynamic_cast<Sphere*>(obj2);

	if (box != nullptr && sphere != nullptr)
	{
		// Checking first condition
		glm::vec2 circlePos = sphere->getPosition() - box->getPosition();
		float w2 = box->getWidth() / 2, h2 = box->getHeight() / 2;

		int numContacts = 0;
		glm::vec2 contact(0, 0); // contact is in our box coordintates

		//check the four corners to see if any of them are inside the circle
		for (float x = -w2; x <= w2; x += box->getWidth())
		{
			for (float y = -h2; y <= h2; y += box->getHeight())
			{
				glm::vec2 p = x * box->getLocalX() + y * box->getLocalY();
				glm::vec2 dp = p - circlePos;

				if (dp.x * dp.x + dp.y * dp.y < sphere->getRadius() * sphere->getRadius())
				{
					numContacts++;
					contact += glm::vec2(x, y);
				}
			}
		}

		// Checkeing second condition
		glm::vec2* direction = nullptr;

		// Get the local positon of the circle centre
		glm::vec2 localPos(glm::dot(box->getLocalX(), circlePos), 
			glm::dot(box->getLocalY(), circlePos));

		if (localPos.y < h2 && localPos.y > -h2) 
		{
			if (localPos.x > 0 && localPos.x < w2 + sphere->getRadius())
			{
				numContacts++;
				contact += glm::vec2(w2, localPos.y);
				direction = new glm::vec2(box->getLocalX());
			}			
			if (localPos.x < 0 && localPos.x > -(w2 + sphere->getRadius()))
			{
				numContacts++;
				contact += glm::vec2(-w2, localPos.y);
				direction = new glm::vec2(-box->getLocalX());
			}
		}		

		if (localPos.x < w2 && localPos.x > -w2) 
		{
			if (localPos.y > 0 && localPos.y < h2 + sphere->getRadius())
			{
				numContacts++;
				contact += glm::vec2(localPos.x, h2);
				direction = new glm::vec2(box->getLocalY());
			}			
			if (localPos.y < 0 && localPos.y > -(h2 + sphere->getRadius()))
			{
				numContacts++;
				contact += glm::vec2(localPos.x, -h2);
				direction = new glm::vec2(-box->getLocalY());
			}
		}

		if (numContacts > 0)
		{
			contact = box->getPosition() + (1.0f / numContacts) *
				(box->getLocalX() * contact.x + box->getLocalY() * contact.y);

			// With the contact point we can find a penetration vector
			float pen = sphere->getRadius() - glm::length(contact - sphere->getPosition());
			glm::vec2 norm = glm::normalize(sphere->getPosition() - contact);
			glm::vec2 penVec = glm::normalize(contact - sphere->getPosition()) * pen;

			box->resolveCollision(sphere, contact, direction);
			//ApplyContactForces(box, sphere, norm, pen);
			
		}
		delete direction;
	}
	return false;
}

bool PhysicsScene::box2Box(PhysicsObject * obj1, PhysicsObject * obj2)
{
	Box* box1 = dynamic_cast<Box*>(obj1);
	Box* box2 = dynamic_cast<Box*>(obj2);

	if (box1 != nullptr && box2 != nullptr)
	{
		glm::vec2 boxPos = box2->getCenter() - box1->getCenter();

		glm::vec2 normal(0, 0);
		glm::vec2 contact(0, 0);
		float pen = 0;
		int numContacts = 0;

		box1->checkBoxCorners(*box2, contact, numContacts, pen, normal);

		if (box2->checkBoxCorners(*box1, contact, numContacts, pen, normal)) 
		{
			normal = -normal;
		}

		if (pen > 0)
		{
			box1->resolveCollision(box2, contact / float(numContacts), &normal);

			ApplyContactForces(box1, box2, normal, pen);
		}	
		return true;
	}
	return false;
}
#pragma endregion









