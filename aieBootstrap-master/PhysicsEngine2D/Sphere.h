#pragma once
#include "RigidBody.h"

class Sphere : public RigidBody {
public:
	Sphere(glm::vec2 position,
		float radius);

	~Sphere();

	virtual void makeGizmo();
	virtual bool checkCollision(PhysicsObject* pOther);

	float getRadius() { return m_radius; }
	void setRadius(float value) { m_radius = value; setMoment(); }

	glm::vec4 getColour() { return m_colour; }
	void setColour(glm::vec4 value) { m_colour = value; }

	virtual glm::vec2 toWorld(glm::vec2 other);

protected:
	float m_radius;
	glm::vec4 m_colour;

	virtual void setMoment() { m_moment = 0.5f * m_mass * m_radius * m_radius; }
};