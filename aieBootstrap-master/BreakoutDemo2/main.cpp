#include "BreakoutDemo2App.h"

int main() {
	
	// allocation
	auto app = new BreakoutDemo2App();

	// initialise and loop
	app->run("AIE", 1280, 720, false);

	// deallocation
	delete app;

	return 0;
}