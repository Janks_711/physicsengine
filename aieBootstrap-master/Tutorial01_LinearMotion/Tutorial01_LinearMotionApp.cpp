#include "Tutorial01_LinearMotionApp.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include <Gizmos.h>
#include <iostream>

Tutorial01_LinearMotionApp::Tutorial01_LinearMotionApp() {

}

Tutorial01_LinearMotionApp::~Tutorial01_LinearMotionApp() {

}

bool Tutorial01_LinearMotionApp::startup() {
	// Increase the 2d line count to maximize the number of objects we can draw
	aie::Gizmos::create(255U, 255U, 65535U, 65535U);

	m_2dRenderer = new aie::Renderer2D();

	// TODO: remember to change this when redistributing a build!
	// the following path would be used instead: "./font/consolas.ttf"
	m_font = new aie::Font("../bin/font/consolas.ttf", 32);

	m_physicsScene = new PhysicsScene();
	m_physicsScene->setTimeStep(0.01f);
	m_physicsScene->setCollisionsEnabled(false);
	m_physicsScene->setGravity(glm::vec2(0, 0));

	//Setup Rocket Test
	m_forceToAdd = 50.0f;
	m_timeInterval = .1f;
	m_counter = m_timeInterval;
	m_rocket = new Sphere(glm::vec2(0, -50), glm::vec2(0, 0), 1000, 10, 
		glm::vec4(1, 0, 0, 1));
	m_physicsScene->addActor(m_rocket);

	return true;
}

void Tutorial01_LinearMotionApp::shutdown() {

	delete m_font;
	delete m_2dRenderer;
	delete m_physicsScene;
}

void Tutorial01_LinearMotionApp::update(float deltaTime) {

	// input example
	aie::Input* input = aie::Input::getInstance();

	aie::Gizmos::clear();

	m_counter -= 1.0f * deltaTime;

	std::cout << m_counter << std::endl;
	std::cout << "Mass = " << m_rocket->getMass() << std::endl;

	// At set time interval reduce mass if above 0 and create a small sphere
	if (m_counter <= 0.0f)
	{
		float mass = m_rocket->getMass() - 10.0f;

		// Only reduce mass if above 0
		if (m_rocket->getMass() >= 10.0f + 1.f)
		{
			m_rocket->setMass(mass);

			Sphere* exhaust = new Sphere(m_rocket->getPosition() + glm::vec2(0.f, -m_rocket->getRadius()), glm::vec2(0, 0), 
				mass, .5, glm::vec4(0, 1, 0, 1));
			m_physicsScene->addActor(exhaust);
			exhaust->applyForceToActor(m_rocket, glm::vec2(0, m_forceToAdd));
		}

		// reset counter
		m_counter = m_timeInterval;
	}

	m_rocket->applyForce(glm::vec2(0, m_forceToAdd));

	m_physicsScene->update(deltaTime);
	m_physicsScene->updateGizmos();

	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void Tutorial01_LinearMotionApp::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();

	// draw your stuff here!
	// draw your stuff here!
	static float aspectRatio = 16 / 9.f;
	aie::Gizmos::draw2D(glm::ortho<float>(-100, 100, -100 / aspectRatio, 
		100 / aspectRatio, -1.0f, 1.0f));

	// output some text, uses the last used colour
	m_2dRenderer->drawText(m_font, "Press ESC to quit", 0, 0);

	// done drawing sprites
	m_2dRenderer->end();
}


void Tutorial01_LinearMotionApp::CollisionTest()
{
	m_physicsScene->setGravity(glm::vec2(0, 0));

	Sphere* ball1 = new Sphere(glm::vec2(-20, 0), glm::vec2(0, 0), 4.0f, 4, glm::vec4(1, 0, 0, 1));
	Sphere* ball2 = new Sphere(glm::vec2(10, 0), glm::vec2(0, 0), 4.0f, 4, glm::vec4(0, 1, 0, 1));

	m_physicsScene->addActor(ball1);
	m_physicsScene->addActor(ball2);

	ball1->applyForce(glm::vec2(30, 0));
	ball2->applyForce(glm::vec2(-15, 0));
}

void Tutorial01_LinearMotionApp::NewtonsThirdLaw()
{
	m_physicsScene->setGravity(glm::vec2(0, 0));

	Sphere* ball1 = new Sphere(glm::vec2(-4, 0), glm::vec2(0, 0), 4.0f, 4, glm::vec4(1, 0, 0, 1));
	Sphere* ball2 = new Sphere(glm::vec2(4, 0), glm::vec2(0, 0), 4.0f, 4, glm::vec4(0, 1, 0, 1));

	m_physicsScene->addActor(ball1);
	m_physicsScene->addActor(ball2);

	ball1->applyForceToActor(ball2, glm::vec2(2, 0));
}

void Tutorial01_LinearMotionApp::NewtonsFirstASecondDemo()
{
	m_physicsScene->setGravity(glm::vec2(0, -10));
	Sphere* ball;
	ball = new Sphere(glm::vec2(-40, 0), glm::vec2(10, 30), 3.0f, 1, glm::vec4(1, 0, 0, 1));
	m_physicsScene->addActor(ball);
}