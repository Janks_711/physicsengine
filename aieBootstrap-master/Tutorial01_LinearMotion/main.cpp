#include "Tutorial01_LinearMotionApp.h"

int main() {
	
	// allocation
	auto app = new Tutorial01_LinearMotionApp();

	// initialise and loop
	app->run("AIE", 1280, 720, false);

	// deallocation
	delete app;

	return 0;
}