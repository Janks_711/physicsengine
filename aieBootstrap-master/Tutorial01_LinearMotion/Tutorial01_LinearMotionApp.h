#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "PhysicsScene.h"
#include "Sphere.h"

class Tutorial01_LinearMotionApp : public aie::Application {
public:

	Tutorial01_LinearMotionApp();
	virtual ~Tutorial01_LinearMotionApp();

	virtual bool startup();
	virtual void shutdown();

	void CollisionTest();
	void NewtonsThirdLaw();
	void NewtonsFirstASecondDemo();

	virtual void update(float deltaTime);
	virtual void draw();

protected:

	aie::Renderer2D*	m_2dRenderer;
	aie::Font*			m_font;
	PhysicsScene*		m_physicsScene;
	Sphere*				m_rocket;	
	float				m_timeInterval;
	float				m_counter;
	float				m_forceToAdd;
};