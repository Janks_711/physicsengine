#include "Tutorial03_CollisionDetectionApp.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include <glm\ext.hpp>
#include <Gizmos.h>

Tutorial03_CollisionDetectionApp::Tutorial03_CollisionDetectionApp() {

}

Tutorial03_CollisionDetectionApp::~Tutorial03_CollisionDetectionApp() {

}

bool Tutorial03_CollisionDetectionApp::startup() {
	
	// Increase the 2d line count to maximize the number of objects we can draw
	aie::Gizmos::create(255U, 255U, 65535U, 65535U);

	m_2dRenderer = new aie::Renderer2D();

	// TODO: remember to change this when redistributing a build!
	// the following path would be used instead: "./font/consolas.ttf"
	m_font = new aie::Font("../bin/font/consolas.ttf", 32);

	m_physicsScene = new PhysicsScene();
	m_physicsScene->setTimeStep(0.01f);

	//Set Gravity to 0
	m_physicsScene->setGravity(glm::vec2(0, 0));

	// Turn collisions off
	m_physicsScene->setCollisionsEnabled(false);

	Sphere* sphere1 = new Sphere(glm::vec2(20, 10), glm::vec2(10, 0), 1, 5, glm::vec4(1, 0, 0, 1));
	Sphere* sphere2 = new Sphere(glm::vec2(40, 10), glm::vec2(-10, 0), 1, 5, glm::vec4(0, 1, 0, 1));

	m_physicsScene->addActor(sphere1);
	m_physicsScene->addActor(sphere2);

	Sphere* sphere3 = new Sphere(glm::vec2(20, 0), glm::vec2(-10, 0), 1, 5, glm::vec4(1, 0, 0, 1));
	Plane* plane = new Plane(glm::vec2(1, 0), -20);

	m_physicsScene->addActor(sphere3);
	m_physicsScene->addActor(plane);

	return true;
}

void Tutorial03_CollisionDetectionApp::shutdown() {

	delete m_font;
	delete m_2dRenderer;
}

void Tutorial03_CollisionDetectionApp::update(float deltaTime) {

	// input example
	aie::Input* input = aie::Input::getInstance();

	aie::Gizmos::clear();

	m_physicsScene->update(deltaTime);
	m_physicsScene->updateGizmos();
	m_physicsScene->checkForCollision();

	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void Tutorial03_CollisionDetectionApp::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();

	// draw your stuff here!
	static float aspectRatio = 16 / 9.f;
	aie::Gizmos::draw2D(glm::ortho<float>(-100, 100, -100 / aspectRatio, 100 / aspectRatio, -1.0f, 1.0f));

	// output some text, uses the last used colour
	m_2dRenderer->drawText(m_font, "Press ESC to quit", 0, 0);

	// done drawing sprites
	m_2dRenderer->end();
}