#include "Tutorial03_CollisionDetectionApp.h"

int main() {
	
	// allocation
	auto app = new Tutorial03_CollisionDetectionApp();

	// initialise and loop
	app->run("AIE", 1280, 720, false);

	// deallocation
	delete app;

	return 0;
}