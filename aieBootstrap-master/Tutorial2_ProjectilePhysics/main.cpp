#include "Tutorial2_ProjectilePhysicsApp.h"

int main() {
	
	// allocation
	auto app = new Tutorial2_ProjectilePhysicsApp();

	// initialise and loop
	app->run("AIE", 1920, 1080, false);

	// deallocation
	delete app;

	return 0;
}