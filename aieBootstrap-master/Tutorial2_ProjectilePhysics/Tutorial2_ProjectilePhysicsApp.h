#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "PhysicsScene.h"
#include <glm/glm.hpp>

#define M_PI 3.1415926535897932384626433832795

class Tutorial2_ProjectilePhysicsApp : public aie::Application {
public:

	Tutorial2_ProjectilePhysicsApp();
	virtual ~Tutorial2_ProjectilePhysicsApp();

	virtual bool startup();
	virtual void shutdown();

	void setupConinuousDemo(glm::vec2 startPos, float inclination, 
		float speed, float gravity);

	virtual void update(float deltaTime);
	virtual void draw();

protected:

	aie::Renderer2D*	m_2dRenderer;
	aie::Font*			m_font;
	PhysicsScene*		m_physicsScene;
	float count;
};