#include "Tutorial2_ProjectilePhysicsApp.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include "Sphere.h"
#include <Gizmos.h>
#include <math.h>

Tutorial2_ProjectilePhysicsApp::Tutorial2_ProjectilePhysicsApp() {

}

Tutorial2_ProjectilePhysicsApp::~Tutorial2_ProjectilePhysicsApp() {

}

bool Tutorial2_ProjectilePhysicsApp::startup() {

	// Increase the 2d line count to maximize the number of objects we can draw
	aie::Gizmos::create(255U, 255U, 65535U, 65535U);
	
	m_2dRenderer = new aie::Renderer2D();

	// TODO: remember to change this when redistributing a build!
	// the following path would be used instead: "./font/consolas.ttf"
	m_font = new aie::Font("../bin/font/consolas.ttf", 32);

	m_physicsScene = new PhysicsScene();
	m_physicsScene->setCollisionsEnabled(false);
	m_physicsScene->setGravity(glm::vec2(0, -10));
	m_physicsScene->setTimeStep(0.5f);

	count = 0;

	float radius = 1;
	float speed = 20.0f;
	glm::vec2 startPos = glm::vec2(-40.f, 0.f);
	float gravity = -10.0f;
	float inclination = (float)M_PI / 4.0f;

	setupConinuousDemo(startPos, inclination, speed, gravity);

	m_physicsScene->addActor(new Sphere(startPos, glm::vec2(inclination, speed), 1, radius,
		glm::vec4(1, 0, 0, 1)));

	return true;
}

void Tutorial2_ProjectilePhysicsApp::shutdown() {

	delete m_font;
	delete m_2dRenderer;
	delete m_physicsScene;
}

void Tutorial2_ProjectilePhysicsApp::update(float deltaTime) {

	// input example
	aie::Input* input = aie::Input::getInstance();

	m_physicsScene->update(deltaTime);
	m_physicsScene->updateGizmos();


	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void Tutorial2_ProjectilePhysicsApp::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();

	// draw your stuff here!
	static float aspectRatio = 16 / 9.f;
	aie::Gizmos::draw2D(glm::ortho<float>(-100, 100, -100 / aspectRatio, 100 / aspectRatio, -1.0f, 1.0f));

	// output some text, uses the last used colour
	m_2dRenderer->drawText(m_font, "Press ESC to quit", 0, 0);

	// done drawing sprites
	m_2dRenderer->end();
}

void Tutorial2_ProjectilePhysicsApp::setupConinuousDemo(glm::vec2 startPos, float inclination,
	float speed, float gravity)
{
	float t = 0;
	float tStep = 0.5f;
	float radius = 1.0f;
	int segments = 12;
	glm::vec4 colour = glm::vec4(1, 1, 0, 1);

	while (t <= 5)
	{
		// Calculate for x
		float x = startPos.x + speed * t;

		// Calculate for y
		float y = startPos.y + inclination * t + (gravity * (t * t)) / 2;

		aie::Gizmos::add2DCircle(glm::vec2(x, y), radius, segments, colour);
		t += tStep;
	}
}