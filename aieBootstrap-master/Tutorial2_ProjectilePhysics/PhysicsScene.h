#pragma once
#include <glm/ext.hpp>
#include <vector>

class PhysicsObject;

/// <SUMMARY> ////
// This class will control the simulation and drawing of all physical objects in our game.

class PhysicsScene
{
public:
	PhysicsScene();
	~PhysicsScene();

	void addActor(PhysicsObject* actor);
	void removeActor(PhysicsObject* actor);
	void update(float dt);
	void updateGizmos();
	void debugScene();

	void setGravity(const glm::vec2 gravity) { m_gravity = gravity; }
	glm::vec2 getGravity() const { return m_gravity; }

	void setTimeStep(const float timeStep) { m_timeStep = timeStep; }
	float getTimeStep() const { return m_timeStep; }

	void setCollisionsEnabled(bool value) { collisionsEnabled = value; }
	bool getCollsionsEnabled() { return collisionsEnabled; }

protected:
	glm::vec2 m_gravity;
	float m_timeStep;
	std::vector<PhysicsObject*> m_actors;
	bool collisionsEnabled;
};

