#include "Sphere.h"
#include <Gizmos.h>

Sphere::Sphere(glm::vec2 position, glm::vec2 velocity, float mass, 
	float radius, glm::vec4 colour) :
	RigidBody(SPHERE, position, velocity, 0, mass, 0.3f, 0.3f), m_radius(radius), m_colour(colour)
{
}

Sphere::~Sphere()
{
}

void Sphere::makeGizmo()
{
	aie::Gizmos::add2DCircle(m_position, m_radius, 100, m_colour);
}

bool Sphere::checkCollision(PhysicsObject * pOther)
{
	Sphere* oSphere = dynamic_cast<Sphere*>(pOther);

	if (oSphere) {
		float distance = glm::distance(this->m_position, oSphere->getPosition());
		float combineRad = this->m_radius + oSphere->getRadius();

		if (distance < combineRad)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	return false;
}
