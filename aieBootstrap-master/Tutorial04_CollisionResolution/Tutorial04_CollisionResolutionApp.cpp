#include "Tutorial04_CollisionResolutionApp.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include <glm\ext.hpp>
#include <Gizmos.h>

Tutorial04_CollisionResolutionApp::Tutorial04_CollisionResolutionApp() {

}

Tutorial04_CollisionResolutionApp::~Tutorial04_CollisionResolutionApp() {

}

bool Tutorial04_CollisionResolutionApp::startup() {
	
	// Increase the 2d line count to maximize the number of objects we can draw
	aie::Gizmos::create(255U, 255U, 65535U, 65535U);

	m_2dRenderer = new aie::Renderer2D();

	// TODO: remember to change this when redistributing a build!
	// the following path would be used instead: "./font/consolas.ttf"
	m_font = new aie::Font("../bin/font/consolas.ttf", 32);

	m_physicsScene = new PhysicsScene();
	m_physicsScene->setTimeStep(0.01f);

	//Set Gravity to 0
	m_physicsScene->setGravity(glm::vec2(0, 0));

	// Turn collisions off
	m_physicsScene->setCollisionsEnabled(false);

	PoolShot1();
	//PoolShot2();
	//PlaneTest();

	return true;
}

void Tutorial04_CollisionResolutionApp::shutdown() {

	delete m_font;
	delete m_2dRenderer;
	delete m_physicsScene;
}

void Tutorial04_CollisionResolutionApp::update(float deltaTime) {

	// input example
	aie::Input* input = aie::Input::getInstance();

	aie::Gizmos::clear();

	m_physicsScene->update(deltaTime);
	m_physicsScene->updateGizmos();
	m_physicsScene->checkForCollision();

	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void Tutorial04_CollisionResolutionApp::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();

	// draw your stuff here!
	// draw your stuff here!
	static float aspectRatio = 16 / 9.f;
	aie::Gizmos::draw2D(glm::ortho<float>(-100, 100, -100 / aspectRatio, 100 / aspectRatio, -1.0f, 1.0f));

	// output some text, uses the last used colour
	m_2dRenderer->drawText(m_font, "Press ESC to quit", 0, 0);

	// done drawing sprites
	m_2dRenderer->end();
}

void Tutorial04_CollisionResolutionApp::PoolShot1()
{
	// Pool Simulation
	Sphere* cueBall = new Sphere(glm::vec2(-30, -5), glm::vec2(21.11, 0), 170, 5, glm::vec4(1, 1, 1, 1));
	Sphere* redBall = new Sphere(glm::vec2(0, 0), glm::vec2(0, 0), 160, 5, glm::vec4(1, 0, 0, 1));

	m_physicsScene->addActor(cueBall);
	m_physicsScene->addActor(redBall);

	cueBall->setElasticity(1.f);
	redBall->setElasticity(0.8f);
}

void Tutorial04_CollisionResolutionApp::PoolShot2()
{
	float degree = (float)M_PI / 1.f;

	Sphere* cueBall = new Sphere(glm::vec2(-20, -10), glm::vec2(11.11f, degree), 170, 5, glm::vec4(1, 1, 1, 1));
	Sphere* redBall = new Sphere(glm::vec2(0, 0), glm::vec2(0, 0), 160, 5, glm::vec4(1, 0, 0, 1));

	m_physicsScene->addActor(cueBall);
	m_physicsScene->addActor(redBall);
}

void Tutorial04_CollisionResolutionApp::PlaneTest()
{
	Sphere* cueBall = new Sphere(glm::vec2(0, 0), glm::vec2(3, 10), 170, 5, glm::vec4(1, 1, 1, 1));
	Plane* plane = new Plane(glm::vec2(-0.65f, 0.75f), 20);

	m_physicsScene->addActor(cueBall);
	m_physicsScene->addActor(plane);
}