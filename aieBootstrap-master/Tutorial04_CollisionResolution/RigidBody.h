#pragma once
#include "PhysicsObject.h"

class RigidBody : public PhysicsObject {
public:
	RigidBody(ShapeType shapeID, glm::vec2 position, glm::vec2 velocity,
		float rotation, float mass, float linearDrag, float angularDrag);
	~RigidBody();

	virtual void fixedUpdate(glm::vec2 gravity, float timeStep);
	virtual void debug();
	void applyForce(glm::vec2 force);
	void applyForceToActor(RigidBody* actor2, glm::vec2 force);

	virtual bool checkCollision(PhysicsObject* pOther) = 0;
	void resolveCollision(RigidBody* actor2);

	glm::vec2 getPosition() { return m_position; }
	float getRotation() { return m_rotation; }

	glm::vec2 getVelocity() { return m_velocity; }
	void setVelocity(glm::vec2 value) { m_velocity = value; }

	float getMass() { return m_mass; }
	void setMass(float value) { m_mass = value; }	
	
	float getLinearDrag() { return m_linearDrag; }
	void setLinearDrag(float value) { m_linearDrag = value; }
	
	float getAngularDrag() { return m_angularDrag; }
	void setAngularDrag(float value) { m_angularDrag = value; }	
	
	float getElasticity() { return m_elasticity; }
	void setElasticity(float value) { m_elasticity = value; }

protected:
	glm::vec2 m_position;
	glm::vec2 m_velocity;
	float m_mass;
	float m_rotation;
	float m_linearDrag;
	float m_angularDrag;
	float m_elasticity;

private:
	static const float MIN_LINEAR_THRESHOLD;
	static const float MIN_ROTATIONAL_THRESHOLD;
}; 