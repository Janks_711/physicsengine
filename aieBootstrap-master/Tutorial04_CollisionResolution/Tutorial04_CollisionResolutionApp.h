#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "PhysicsScene.h"
#include "Sphere.h"
#include "Plane.h"

#define M_PI 3.1415926535897932384626

class Tutorial04_CollisionResolutionApp : public aie::Application {
public:

	Tutorial04_CollisionResolutionApp();
	virtual ~Tutorial04_CollisionResolutionApp();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

	void PoolShot1();
	void PoolShot2();
	void PlaneTest();

protected:

	aie::Renderer2D*	m_2dRenderer;
	aie::Font*			m_font;
	PhysicsScene*		m_physicsScene;
};