#include "Tutorial04_CollisionResolutionApp.h"

int main() {
	
	// allocation
	auto app = new Tutorial04_CollisionResolutionApp();

	// initialise and loop
	app->run("AIE", 1280, 720, false);

	// deallocation
	delete app;

	return 0;
}